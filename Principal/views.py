from django.shortcuts import render

# Create your views here.

from django.views import View

class Principal(View):
    
    def get(self, request):
        
        datos={
            
            'titulo': 'General',
            'encabezado': 'MMD',
            'bienvenida': 'Seguridad para todos'
        }
        
        return render(request, 'principaal/Tprincipaal.html', datos)
    
    